﻿using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static void LoadNextLevel(MonoBehaviour current, float delay=3.0f)
    {
        int index = SceneManager.GetActiveScene().buildIndex+1;
        if (index>SceneManager.sceneCountInBuildSettings)
        {
            index = 0;
        }
        current.StartCoroutine(LoadYourAsyncScene(index,delay));
    }

    public static void RestartLevel(MonoBehaviour current, float delay=3.0f)
    {
        int index = SceneManager.GetActiveScene().buildIndex;
        current.StartCoroutine(LoadYourAsyncScene(index,delay));
    }

    static IEnumerator LoadYourAsyncScene(int index, float delay)
    {
        yield return new  WaitForSeconds(delay);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(index);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
