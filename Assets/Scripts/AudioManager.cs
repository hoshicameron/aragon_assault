﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    #region Declare Variables
    private static AudioManager current;
    [Header("Ambient Audio")]
    public AudioClip ambientClip;
    public AudioClip[] levelMusicClips;

    [Header("Sting")]
    public AudioClip levelStingClip;
    public AudioClip winStingClip;
    public AudioClip looseStingClip;
    public AudioClip fuelStingClip;

    [Header("Ship")]
    public AudioClip thrustClip;
    public AudioClip explodeClip;


    [Header("Mixer Groups")]
    public AudioMixerGroup ambientGroup;
    public AudioMixerGroup musicGroup;
    public AudioMixerGroup stingGroup;
    public AudioMixerGroup shipGroup;

    [Header("Main Audio Mixer")]
    public AudioMixer mainAudioMixer;

    private AudioSource ambientSource;
    private AudioSource stingSource;
    private AudioSource musicSource;
    private AudioSource shipSource;
    #endregion

    private void Awake()
    {
        //If an AudioManager exists and it is not this...
        if (current != null && current != this)
        {
            //...destroy this. There can be only one AudioManager
            Destroy(gameObject);
            Destroy(gameObject);
            return;
        }
        //This is the current AudioManager and it should persist between scene loads
    current = this;
    DontDestroyOnLoad(gameObject);

    //Generate the Audio Source "channels" for our game's audio
    ambientSource=gameObject.AddComponent<AudioSource>()as AudioSource;
    stingSource=gameObject.AddComponent<AudioSource>()as AudioSource;
    musicSource=gameObject.AddComponent<AudioSource>()as AudioSource;
    shipSource=gameObject.AddComponent<AudioSource>()as AudioSource;

    //Assign each audio source to its respective mixer group so that it is
    //routed and controlled by the audio mixer
    ambientSource.outputAudioMixerGroup = ambientGroup;
    stingSource.outputAudioMixerGroup   = stingGroup;
    musicSource.outputAudioMixerGroup   = musicGroup;
    shipSource.outputAudioMixerGroup    = shipGroup;

    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        StartLevelAudio(scene);
    }
    void StartLevelAudio(Scene scene)
    {
        /*//Set the clip for ambient audio, tell it to loop, and then tell it to play
        if (scene.buildIndex >1)
        {
            current.ambientSource.clip = current.ambientClip;
            current.ambientSource.loop = true;
            current.ambientSource.Play();
        }*/

        //Set the clip for music audio, tell it to loop, and then tell it to play
        AudioClip musicClip = current.levelMusicClips[scene.buildIndex];
        if (musicClip)
        {
            current.musicSource.clip = musicClip;
            current.musicSource.loop = true;
            current.musicSource.Play();
        }else
        {
            Debug.LogError("Level music clip is mising...");
        }

    }

    private void Start()
    {
        /*current.mainAudioMixer.SetFloat("MasterVolume", PlayerPrefsManager.GetMasterVolume());
        current.mainAudioMixer.SetFloat("SFXVolume", PlayerPrefsManager.GetSFXVolume());
        current.mainAudioMixer.SetFloat("MusicVolume", PlayerPrefsManager.GetMusicVolume());*/
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public static void PlayShipThrustAudio()
    {
        //If there is no current AudioManager, exit
        if (current == null)
            return;
        if(current.shipSource.isPlaying)
            return;

        //Set the jump SFX clip and tell the source to play
        current.shipSource.clip = current.thrustClip;
        current.shipSource.Play();

    }
    public static void StopShipThrustAudio()
    {
        //If there is no current AudioManager, exit
        if (current == null)
            return;
        //Set the jump SFX clip and tell the source to play
        current.shipSource.Stop();

    }

    public static void PlayShipExplodeAudio()
    {
        //If there is no current AudioManager, exit
        if (current == null)
            return;
       //Set the jump SFX clip and tell the source to play
        current.shipSource.clip = current.explodeClip;
        current.shipSource.Play();

    }
    public static void PlayLevelCompleteAudio()
    {
        //If there is no current AudioManager, exit
        if (current == null)
            return;
        //Set the jump SFX clip and tell the source to play
        current.stingSource.clip = current.winStingClip;
        current.stingSource.Play();

    }
}
