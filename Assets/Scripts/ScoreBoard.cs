﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreBoard : MonoBehaviour
{
    private TMP_Text scoreText;
    private int score;

    private void Start()
    {
        scoreText = GetComponent<TMP_Text>();
        scoreText.text = score.ToString();
    }

    public void AddScore(int scoreHit)
    {
        print("Hitted score triggered!");
        score += scoreHit;
        scoreText.text = score.ToString();
    }
}
