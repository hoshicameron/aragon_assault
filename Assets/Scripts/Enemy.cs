﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Enemy : MonoBehaviour
{
    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] private Transform parent;
    [SerializeField] private ScoreBoard scoreBoard;
    [SerializeField] private int enemyDeathScore=10;
    [SerializeField] private int hits=5;


    private Collider boxCollider;
    // Start is called before the first frame update
    void Start()
    {
        AddNonTriggerBoxCollider();
    }

    private void AddNonTriggerBoxCollider()
    {
        boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnParticleCollision(GameObject other)
    {
        hits--;
        if (hits<1)
        {
            KillEnemy();
        }

    }

    private void KillEnemy()
    {
        GameObject explosion =
            Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
        explosion.transform.parent = parent;
        scoreBoard.AddScore(enemyDeathScore);
        Destroy(gameObject);
    }
}
