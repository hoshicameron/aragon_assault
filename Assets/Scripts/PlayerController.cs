﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityStandardAssets.CrossPlatformInput;

[DisallowMultipleComponent]
public class PlayerController : MonoBehaviour
{
    //todo Why player ship start with alot of speed
    #region Declare Variables
    [Header("General")][Tooltip("In ms^-1")]
    [SerializeField]private float speed = 30.0f;
    [SerializeField] private float xRange;
    [SerializeField] private float yRange;

    [Header("Control-position Based")]
    [SerializeField] private float positionPitchFactor;
    [SerializeField] private float positionYawFactor;

    [Header("control-throw Based")]
    [SerializeField] private float controlRollFactor = -15;
    [FormerlySerializedAs("contrlPitchFactor")] [SerializeField] private float controlPitchFactor = -15;
    [SerializeField] private ParticleSystem[] guns;

    private float horizontal, vertical;
    private bool isControllerEnabeld = true;


    #endregion

    // Update is called once per frame
    void Update()
    {
        if(!isControllerEnabeld)    return;
        ProcessTranslation();
        ProcessRotation();
        ProcessFiring();
    }

    private void ProcessFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            SetGunActive(true);
        } else
        {
            SetGunActive(false);
        }
    }



    private void SetGunActive(bool isActive)
    {
        for (int i = 0; i < guns.Length; i++)
        {
            var em= guns[i].emission;
            em.enabled = isActive;
        }
    }

    private void ProcessRotation()
    {
        //Rotation Coupling
        //Couple pitch to the Position on screen and control throw
        float pitchDueToPosition=transform.localPosition.y*positionPitchFactor;
        float pitchDueToControlThrow = vertical * controlPitchFactor;
        float pitch=pitchDueToPosition+pitchDueToControlThrow;

        //Couple yaw to the Position on screen
        float yaw=transform.localPosition.x * positionYawFactor;

        //Couple roll to the control throw
        float roll=horizontal*controlRollFactor;

        transform.localRotation=Quaternion.Euler(pitch,yaw,roll);
    }

    private void ProcessTranslation()
    {
        horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        vertical = CrossPlatformInputManager.GetAxis("Vertical");

        float xOffset = horizontal * speed * Time.deltaTime;
        float yOffset = vertical * speed * Time.deltaTime;

        var localPosition = transform.localPosition;
        xOffset += localPosition.x;
        yOffset += localPosition.y;

        //Clamp ship Y and X position to avoid it get out of screen
        xOffset = Mathf.Clamp(xOffset, -xRange, xRange);
        yOffset = Mathf.Clamp(yOffset, -yRange, yRange);

        localPosition = new Vector3(xOffset, yOffset, localPosition.z);
        transform.localPosition = localPosition;
    }

    public void OnPlayerDeath()
    {
        isControllerEnabeld = false;
    }
}
