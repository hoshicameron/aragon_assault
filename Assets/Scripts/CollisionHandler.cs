﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] private float delay = 1.0f;
    [SerializeField] private GameObject explosionPrefab;
    private void OnTriggerEnter(Collider other)
    {
        StartDeathSequence();

    }

    private void StartDeathSequence()
    {
        explosionPrefab.SetActive(true);
        gameObject.SendMessage("OnPlayerDeath");
        AudioManager.PlayShipExplodeAudio();
        LevelManager.RestartLevel(this,delay);

    }
}
